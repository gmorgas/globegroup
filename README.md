1. Setup .env file to configure database, jwt token </br>
2. Run command to create all dependencies <code>composer install</code> </br>
3. Run command to create database schema <code>bin/console doctrine:schema:create</code> </br>
4. Run fixtures to fill database: <code>bin/console doctrine:fixtures:load</code> </br>
5. Test
