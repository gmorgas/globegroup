<?php

namespace App\DataFixtures;

use App\Entity\Films;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        //roles
        $roleUser = [
            'ROLE_USER'
        ];
        $roleAdmin = [
            'ROLE_ADMIN'
        ];

        //add random films
        for($i =0; $i < 50; $i++) {
            $films = new Films();
            $films->setTitle($faker->sentence(3));
            $films->setDescription($faker->sentence(50));
            $films->setRate(0);
            $manager->persist($films);
        }

        //set admin user
        $user = new User('Admin');
        $user->setPassword($this->encoder->encodePassword($user, 'admin'));
        $user->setUsername('Admin');
        $user->setEmail('admin@admin.com');
        $user->setUserRoles($roleAdmin);
        $manager->persist($user);

        //set test user
        $testUser = new User('Test');
        $testUser->setPassword($this->encoder->encodePassword($testUser, 'test'));
        $testUser->setUsername('Test');
        $testUser->setEmail('test@test.com');
        $testUser->setUserRoles($roleUser);
        $manager->persist($testUser);

        $manager->flush();
    }
}
