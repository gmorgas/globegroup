<?php


namespace App\Controller;

use App\Entity\Films;
use App\Entity\UserRateFilms;
use App\Repository\FilmsRepository;
use App\Repository\UserRateFilmsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostController
 * @package App\Controller
 * @Route("/api", name="post_api")
 */
class FilmController extends ApiController {

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addFilm(Request $request) {

        $request = $this->transformJsonBody($request);
        $title = $request->get('title');
        $description = $request->get('description');

        if (empty($description) || empty($title)){
            return $this->respondValidationError("Invalid Title or Description");
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $film = new Films();
            $film->setTitle($title);
            $film->setDescription($description);
            $film->setRate(0);
            $em->persist($film);
            $em->flush();

            return $this->respondWithSuccess(sprintf('Film %s successfully added', $film->getTitle()));
        } catch(\Exception $e) {
            return $this->respondValidationError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param FilmsRepository $filmsRepository
     * @param EntityManagerInterface $entityManager
     * @param $id
     * @return JsonResponse
     */
    public function updateFilm(Request $request, FilmsRepository $filmsRepository, EntityManagerInterface $entityManager, $id) {
        try {
            $film = $filmsRepository->find($id);

            if (!$film){
                return $this->respondNotFound(sprintf('Film with id = %s is not found', $id));
            }

            $request = $this->transformJsonBody($request);

            if($request->get('title')) {
                $film->setTitle($request->get('title'));
            }

            if($request->get('description')) {
                $film->setDescription($request->get('description'));
            }
            $entityManager->flush();

            return $this->respondWithSuccess(sprintf('Film %s successfully updated', $film->getTitle()));
        } catch (\Exception $e){
            return $this->respondValidationError($e->getMessage());
        }
    }

    /**
     * @param FilmsRepository $filmsRepository
     * @param EntityManagerInterface $entityManager
     * @param $id
     * @return JsonResponse
     */
    public function deleteFilm(FilmsRepository $filmsRepository, EntityManagerInterface $entityManager, $id) {
        try {
            $film = $filmsRepository->find($id);

            if (!$film){
                return $this->respondNotFound(sprintf('Film with id = %s is not found', $id));
            }

            $entityManager->remove($film);
            $entityManager->flush();

            return $this->respondWithSuccess(sprintf('Film %s successfully deleted', $film->getTitle()));
        } catch (\Exception $e){
            return $this->respondValidationError($e->getMessage());
        }
    }

    /**
     * @param FilmsRepository $filmsRepository
     * @param UserRateFilmsRepository $userRateFilmsRepository
     * @return JsonResponse
     */
    public function getFilms(FilmsRepository $filmsRepository, UserRateFilmsRepository $userRateFilmsRepository) {
        try {
            $film = $filmsRepository->findAll();

            if (!$film){
                return $this->respondWithSuccess('Not found any film');
            }

            foreach ($film as $key => $item) {
                $data[$key] = $item->toArray();
                $data[$key]['number_of_votes'] = count($userRateFilmsRepository->findBy(array('films_id' => $item->getId())));
            }

            return $this->response($data);

        } catch(\Exception $e) {
            return $this->respondValidationError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param FilmsRepository $filmsRepository
     * @param UserRateFilmsRepository $userRateFilmsRepository
     * @return JsonResponse
     */
    public function getFilmsWithFilter(Request $request, FilmsRepository $filmsRepository, UserRateFilmsRepository $userRateFilmsRepository) {
        try {
            $request = $this->transformJsonBody($request);
            $limit = '50';
            $filter = array();

            if(!is_numeric($request->get('limit'))) {
                $limit = $request->get('limit');
            }
            if($request->get('title')) {
                $filter['title'] = $request->get('title');
            }
            if($request->get('description')) {
                $filter['description'] = $request->get('description');
            }

            $film = $filmsRepository->findByFilters($filter, $limit);

            if (!$film){
                return $this->respondWithSuccess('Not found any film');
            }

            foreach ($film as $key => $item) {
                $data[$key] = $item->toArray();
                $data[$key]['number_of_votes'] = count($userRateFilmsRepository->findBy(array('films_id' => $item->getId())));
            }

            return $this->response($data);

        } catch(\Exception $e) {
            return $this->respondValidationError($e->getMessage());
        }
    }

    /**
     * @param FilmsRepository $filmsRepository
     * @param UserRateFilmsRepository $userRateFilmsRepository
     * @param $id
     * @return JsonResponse
     */
    public function getFilmDetail(FilmsRepository $filmsRepository, UserRateFilmsRepository $userRateFilmsRepository, $id) {
        try {
            $film = $filmsRepository->find($id);

            if (!$film){
                return $this->respondNotFound(sprintf('Film with id = %s is not found', $id));
            }

            $data = $film->toArray();
            $data['number_of_votes'] = count($userRateFilmsRepository->findBy(array('films_id' => $id)));

            return $this->response($data);

        } catch(\Exception $e) {
            return $this->respondValidationError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param FilmsRepository $filmsRepository
     * @param UserRateFilmsRepository $userRateFilmsRepository
     * @param EntityManagerInterface $entityManager
     * @param $id
     * @return JsonResponse
     */
    public function setFilmRate(Request $request, FilmsRepository $filmsRepository, UserRateFilmsRepository $userRateFilmsRepository, EntityManagerInterface $entityManager, $id) {
        try {
            $film = $filmsRepository->find($id);
            $user = $this->get('security.token_storage')->getToken()->getUser();
            if (!$film){
                return $this->respondNotFound(sprintf('Film with id = %s is not found', $id));
            }

            if(count($userRateFilmsRepository->findBy(array('films_id' => $id, 'user_id' => $user->getId()))) > 0) {
                return $this->respondWithSuccess(sprintf('The users has already voted for the video with id: %s', $film->getId()));
            }
            $request = $this->transformJsonBody($request);

            if(!is_numeric($request->get('rate'))) {
                return $this->respondValidationError("Invalid Rate value");
            }

            $rate = $film->getRate();
            if(count($userRateFilmsRepository->findBy(array('films_id' => $id))) == 0) {
                $rate = $request->get('rate');
            }

            //add vote
            $this->addVoteForFilm($id, $request->get('rate'));
            $film->setRate(round(($rate+$request->get('rate'))/2, 2));
            $entityManager->flush();

            return $this->respondWithSuccess(sprintf('User successfully voted for the movie with id: %s', $film->getId()));
        } catch(\Exception $e) {
            return $this->respondValidationError($e->getMessage());
        }
    }

    /**
     * @param $filmId
     * @param $rate
     * @return bool|JsonResponse
     */
    protected function addVoteForFilm($filmId, $rate) {
        try {
            $em = $this->getDoctrine()->getManager();
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $userVote = new UserRateFilms();
            $userVote->setUserId($user->getId());
            $userVote->setFilmsId($filmId);
            $userVote->setRate($rate);
            $em->persist($userVote);
            $em->flush();
            return true;

        } catch (\Exception $e) {
            return $this->respondValidationError($e->getMessage());
        }
    }
}