<?php

namespace App\Repository;

use App\Entity\Films;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Films|null find($id, $lockMode = null, $lockVersion = null)
 * @method Films|null findOneBy(array $criteria, array $orderBy = null)
 * @method Films[]    findAll()
 * @method Films[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Films::class);
    }

    /**
     * @param $filter
     * @param $limit
     * @return int|mixed|string
     */
    public function findByFilters($filter, $limit) {
        $qb = $this->createQueryBuilder('s');
        if(isset($filter['title'])) {
            $qb->andWhere('s.title LIKE :title')
               ->setParameter('title', '%'.addcslashes($filter['title'], '%_').'%');
        }
        if(isset($filter['description'])) {
            $qb->andWhere('s.description LIKE :description')
               ->setParameter('description', '%'.addcslashes($filter['description'], '%_').'%');
        }
        return $qb->setMaxResults($limit)
                  ->getQuery()
                  ->getResult();
    }

    // /**
    //  * @return Films[] Returns an array of Films objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Films
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
